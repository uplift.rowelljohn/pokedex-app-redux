import { combineReducers } from "redux";
import PokemonTypeListReducer from "./PokemonTypeListReducer"
import PokemonListReducer from "./PokemonListReducer";
import PokemonListByTypeReducer from "./PokemonListByTypeReducer";

const RootReducer = combineReducers({
    PokemonTypeList: PokemonTypeListReducer,
    PokemonList: PokemonListReducer,
    PokemonListByType: PokemonListByTypeReducer
});

export default RootReducer;