
const DefaultState = {
    loading: false,
    data: [],
    errMsg: "",
    count: 0
};

const PokemonTypeListReducer = (state = DefaultState, action) => {
    switch (action.type){
        case "POKEMON_TYPE_LIST_LOADING":
            return{
                ...state,
                loading: true,
                errMsg: ""
            }
        case "POKEMON_TYPE_LIST_FAIL":
            return {
                ...state,
                loading: false,
                errMsg: "Unable to get pokemon types"
            }
        case "POKEMON_TYPE_LIST_SUCCESS":
            return {
                ...state,
                loading: false,
                data: action.payload.results,
                errMsg: "",
                count: action.payload.count
            }
        default:
            return state
    }
};

export default PokemonTypeListReducer;