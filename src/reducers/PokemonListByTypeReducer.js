
const DefaultState = {
    loading: false,
    data: [],
    errMsg: "",
    count: 0
};

const PokemonListByTypeReducer = (state = DefaultState, action) => {
    switch (action.type){
        case "POKEMON_LIST_BY_TYPE_LOADING":
            return{
                ...state,
                loading: true,
                errMsg: ""
            }
        case "POKEMON_LIST_BY_TYPE_FAIL":
            return {
                ...state,
                loading: false,
                errMsg: "Unable to get pokemon types"
            }
        case "POKEMON_LIST_BY_TYPE_SUCCESS":
            return {
                ...state,
                loading: false,
                data: action.payload,
                errMsg: "",
                count: action.payload.count
            }
        default:
            return state
    }
};

export default PokemonListByTypeReducer;