//import { Switch } from '@material-ui/core';
import React from 'react'; 
import { Route, Switch, Redirect } from 'react-router-dom';
import PokedexMain from './components/PokedexMain';
import Pokemon from './components/Pokemon';
import PokemonType from './components/PokemonType';

const App = () => {
  return (
    <div>
       <Switch>
         <Route exact path={"/"}  render={(props) => <PokedexMain {...props} />} />
         <Route exact path={"/:pokemonId"} render={(props) => <Pokemon {...props} />} />
         <Route exact path={"/type/:pokemonType"} render={(props) => <PokemonType {...props} />} />
         <Redirect to={"/"} />
       </Switch>
    </div>
  );
}

/*
  TODO LIST
   - clean code
   - clean styling
   - look for functions for possible separate component
   - review code


   NOTE
   - commented lines of codes are for reference purposes
*/

export default App;
