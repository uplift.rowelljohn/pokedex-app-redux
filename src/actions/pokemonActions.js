import axios from "axios";

export const GetPokemonTypeList = () => async dispatch => {
    try{
        dispatch({
            type: "POKEMON_TYPE_LIST_LOADING"
        });
        const res = await axios.get(`https://pokeapi.co/api/v2/type`)
        dispatch({
            type: "POKEMON_TYPE_LIST_SUCCESS",
            payload: res.data
        });
    }catch(e){
        dispatch({
            type: "POKEMON_TYPE_LIST_FAIL",
        });
    }
};

export const GetPokemonList = () => async dispatch => {
    try {
        dispatch({
            type: "POKEMON_LIST_LOADING"
        });
        // const perPage = 18;
        // const offset = (page * perPage) - perPage;
        // const res = await axios.get(`https://pokeapi.co/api/v2/pokemon?limit=${perPage}&offset=${offset}`)
        
        const res = await axios.get(`https://pokeapi.co/api/v2/pokemon?limit=198`)
        
        dispatch({
            type: "POKEMON_LIST_SUCCESS",
            payload: res.data
        });
    } catch (e) {
        dispatch({
            type: "POKEMON_LIST_FAIL",
        });
    }
};

export const GetPokemonByType = (pokemonType) => async dispatch => {
    try {
        dispatch({
            type: "POKEMON_LIST_BY_TYPE_LOADING"
        });

        const res = await axios.get(`https://pokeapi.co/api/v2/type/${pokemonType}`)
        dispatch({
            type: "POKEMON_LIST_BY_TYPE_SUCCESS",
            payload: res.data.pokemon
        });
    } catch (e) {
        dispatch({
            type: "POKEMON_LIST_BY_TYPE_FAIL",
        });
    }
};