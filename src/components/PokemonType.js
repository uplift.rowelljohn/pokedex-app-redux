import React, { useEffect, useState } from "react";
import { Grid, Card, CardContent, CircularProgress, CardMedia, Typography, TextField } from "@material-ui/core";
import { alpha, makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import Box from '@mui/material/Box';
import DrawerMenu from "./DrawerMenu";
import { styled } from '@mui/material/styles';
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import { bindActionCreators } from 'redux';
import { actionCreators } from '../actions/index';

const useStyles = makeStyles(theme => ({
    pokedexContainer: {
        paddingTop: "20px",
        paddingLeft: "20px",
        paddingRight: "20px",
    },
    card: {
        maxWidth: "300px",
        boxShadow: "0 5px 8px 0 rgba(0, 0, 0, 0.3)",
        backgroundColor: "#fafafa",
        paddingTop: "20px",
        paddingLeft: "20px",
        paddingRight: "20px",
        marginLeft: "10px",
        marginBottom: "20px",
    },
    cardMedia: {
        margin: "auto",
    },
    cardContent: {
        textAlign: "center",
    },
    searchContainer: {
        display: "flex",
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        width: "300px",
        margin: "auto",
        padding: "10px",
    },
    searchIcon: {
        alignSelf: "flex-end",
        marginBottom: "5px",
    },
    searchInput: {
        width: "300px",
        margin: "5px"
    }
}));

const firstCharToUpper = (name) => {
    return name.charAt(0).toUpperCase() + name.slice(1);
}

const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: `-${drawerWidth}px`,
        ...(open && {
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: 0,
        }),
    }),
);

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));

const PokemonType = (props) => {
    const { match, history } = props;
    const { params } = match;
    const { pokemonType } = params;
    const classes = useStyles();
    const [pokemonData, setPokemonData] = useState({});
    const [searchFilter, setSearchFilter] = useState("");

    const onChangeSearchHandler = (e) => {
        setSearchFilter(e.target.value)
    }

    const PokemonListByType = useSelector(state => state.PokemonListByType);

    const dispatch = useDispatch();
    const { GetPokemonByType } = bindActionCreators(actionCreators, dispatch);

    useEffect(() => {
        GetPokemonByType(pokemonType)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pokemonType])

    //pokemonData
    useEffect(() => {
            const newPokemonData = {};
            PokemonListByType.data.forEach((pokemon, index) => {
                const image = pokemon.pokemon.url.split("/");
                newPokemonData[index + 1] = {
                    name: pokemon.pokemon.name,
                    sprite: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${image[image.length - 2]}.png`,
                };
            setPokemonData(newPokemonData);
        });
    }, [PokemonListByType.data]);

    const ShowListPokemons = () => {
        if (!_.isEmpty(PokemonListByType.data)) {
            return <h2 style={{ textAlign: "center" }}>POKEDEX</h2>
        }
        if (PokemonListByType.loading) {
            return <p>Loading pokemon list</p>
        }
        if (PokemonListByType.errMsg !== "") {
            return <p>{PokemonListByType.errMsg}</p>
        }
        return <p>unable to get pokemon list</p>
    }

    const getPokemonCard = (pokemonId) => {
        const { name, sprite } = pokemonData[pokemonId];
        return (
            <Card onClick={() => history.push(`/${name}`)} sx={{ minWidth: 275 }} className={classes.card} key={name}>
                <CardMedia className={classes.cardMedia} image={sprite} style={{ width: "170px", heigth: "auto" }} component="img" />
                <CardContent>
                    <Typography className={classes.cardContent} >{firstCharToUpper(name)}</Typography>
                </CardContent>
            </Card>
        )
    }

    const [open] = React.useState(false);

    return (
    <Box sx={{ display: 'flex' }}>
        
        <DrawerMenu />

        <Main open={open}>
            <DrawerHeader />
            {/* try&catch for loading pokemon & displaying header */}
            {ShowListPokemons()}
            <div className={classes.searchContainer}>
                <SearchIcon className={classes.searchIcon} />
                <TextField onChange={onChangeSearchHandler} className={classes.searchInput} label="Search Pokemon" variant="standard" />
            </div>
            {pokemonData ? (
                    <Grid container className={classes.pokedexContainer} style={{ display: "flex" }}>
                    {
                        Object.keys(pokemonData).map((pokemonId) => (
                            pokemonData[pokemonId].name.includes(searchFilter) &&
                            getPokemonCard(pokemonId)
                        ))
                    }
                </Grid>
            ) : (
                <CircularProgress />
            )}
        </Main>
    </Box>
    );
}
export default PokemonType;