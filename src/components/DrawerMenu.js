import React, { useEffect } from "react";
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from "@mui/material/Toolbar";
import { Link } from 'react-router-dom';
import { styled, useTheme } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import MenuIcon from '@mui/icons-material/Menu';
import MuiAppBar from '@mui/material/AppBar';
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import { bindActionCreators } from 'redux';
import { actionCreators } from '../actions/index';

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
    transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: `${drawerWidth}px`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));

const DrawerMenu = () => {
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const PokemonTypeList = useSelector(state => state.PokemonTypeList);

    const dispatch = useDispatch();
    const { GetPokemonTypeList } = bindActionCreators(actionCreators, dispatch);

    useEffect(() => {
        GetPokemonTypeList()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const ShowPokemonTypes = () => {
        if (!_.isEmpty(PokemonTypeList.data)) {
            return PokemonTypeList.data.map(type => {
                return <div key={type.name}>
                    <ListItem button component={Link} to={`/type/${type.name}`} >
                        <ListItemText  primary={type.name}/>
                    </ListItem>
                </div>
            })
        }
        if (PokemonTypeList.loading) {
            return <p>Loading pokemon types</p>
        }
        if (PokemonTypeList.errMsg !== "") {
            return <p>{PokemonTypeList.errMsg}</p>
        }
        return <p>unable to get pokemon types</p>
    };

    return(
        <>
        <CssBaseline />
        <AppBar position="fixed" open={open}>
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                    sx={{ mr: 2, ...(open && { display: 'none' }) }}
                >
                    <MenuIcon />
                </IconButton>
            </Toolbar>
        </AppBar>
        <Drawer
            sx={{
                width: drawerWidth,
                flexShrink: 0,
                '& .MuiDrawer-paper': {
                    width: drawerWidth,
                    boxSizing: 'border-box',
                },
            }}
            variant="persistent"
            anchor="left"
            open={open}
        >
            <DrawerHeader>
                    POKEDEX
                <IconButton onClick={handleDrawerClose}>
                    {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                </IconButton>
            </DrawerHeader>
            <Divider />
            <List>
                <ListItem button component={Link} to={`/`}>
                    <ListItemText primary={"All"} />
                </ListItem>
                {ShowPokemonTypes()}
            </List>
        </Drawer>
        </>
    )
}

export default DrawerMenu;