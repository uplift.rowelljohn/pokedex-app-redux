import React, { useEffect, useState } from "react";
import { Grid, Card, CardContent, CircularProgress, CardMedia, Typography, TextField} from "@material-ui/core";
import { alpha, makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import Box from '@mui/material/Box';
import DrawerMenu from "./DrawerMenu";
import { styled } from '@mui/material/styles';
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import { bindActionCreators } from 'redux';
import { actionCreators } from '../actions/index';


const useStyles = makeStyles(theme => ({
    pokedexContainer : {
        paddingTop: "20px",
        paddingLeft: "20px",
        paddingRight: "20px",
    },
    card: {
        maxWidth: "300px",
        boxShadow: "0 5px 8px 0 rgba(0, 0, 0, 0.3)",
        backgroundColor: "#fafafa",
        paddingTop: "20px",
        paddingLeft: "20px",
        paddingRight: "20px",
        marginLeft: "10px",
        marginBottom: "20px",
    },
    cardMedia: {
        margin: "auto",
    },
    cardContent: {
         textAlign: "center",
    },
    searchContainer: {
        display: "flex",
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        width: "300px",
        margin: "auto",
        padding: "10px",
    },
    searchIcon: {
        alignSelf: "flex-end",
        marginBottom: "5px",
    },
    searchInput: {
        width: "300px",
        margin: "5px"
    }
})); 

const firstCharToUpper = (name) => {
    return name.charAt(0).toUpperCase() + name.slice(1);
}

const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: `-${drawerWidth}px`,
        ...(open && {
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: 0,
        }),
    }),
);

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));


const PokedexMain = (props) => {
    const { history } = props;
    const classes = useStyles();
    const [pokemonData, setPokemonData] = useState({});
    const [searchFilter, setSearchFilter] = useState("");
    
    const onChangeSearchHandler = (e) => {
        setSearchFilter(e.target.value)
    }

    const PokemonList = useSelector(state => state.PokemonList);
    
    const dispatch = useDispatch();
    const {GetPokemonList} = bindActionCreators(actionCreators, dispatch);

    useEffect(() => {
        GetPokemonList()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        const newPokemonData = {};
        PokemonList.data.forEach((pokemon, index) => {
            newPokemonData[index + 1] = {
                id: index + 1,
                name: pokemon.name,
                sprite: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${index + 1}.png`,
            };
        });
        setPokemonData(newPokemonData);
    }, [PokemonList.data]);
    
    const ShowListPokemons = () => {
        if (!_.isEmpty(PokemonList.data)) {
            return <h2 style={{textAlign:"center"}}>POKEDEX</h2>
        }
        if (PokemonList.loading) {
            return <p>Loading pokemon list</p>
        }
        if (PokemonList.errMsg !== "") {
            return <p>{PokemonList.errMsg}</p>
        }
        return <p>unable to get pokemon list</p>
    }

    const getPokemonCard = (pokemonId) => {
        const { name, sprite } = pokemonData[pokemonId];
        return (
            <Card onClick={() => history.push(`/${name}`)} sx={{ minWidth: 275 }} className={classes.card} key={name}>
                <CardMedia className={classes.cardMedia} image={sprite} style={{ width: "170px", heigth: "auto" }} component="img" />
                <CardContent>
                    <Typography  className={classes.cardContent}>{firstCharToUpper(name)}</Typography>
                </CardContent>
            </Card>
        )
    }
    const [open] = React.useState(false);

    return(
    <Box sx={{ display: 'flex' }}>
        <DrawerMenu />
        <Main open={open}>
            <DrawerHeader />
            {/* try&catch for loading pokemon & displaying header */}
            {ShowListPokemons()}
                    <div className={classes.searchContainer}>
                        <SearchIcon className={classes.searchIcon} />
                        <TextField onChange={onChangeSearchHandler} className={classes.searchInput} label="Search Pokemon" variant="standard" />
                    </div>
            {pokemonData ? (
                    <Grid container className={classes.pokedexContainer} style={{ display: "flex" }}>
                    {
                        Object.keys(pokemonData).map((pokemonId) => (
                            pokemonData[pokemonId].name.includes(searchFilter) &&
                            getPokemonCard(pokemonId)
                        ))
                    }
                </Grid>
            ) : (
                <CircularProgress />
            )}
        </Main>
    </Box>
    );
}
export default PokedexMain;


    // const FetchTypes = () => {
    //     dispatchTypes(GetPokemonTypeList())
    // }
    // const FetchData = () => {
    //     dispatchList(GetPokemonList())
    // }

    //v2
    
    // const dispatchTypes = useDispatch();
    // const dispatchList = useDispatch();

    // useEffect(() => {
    //     dispatchTypes(GetPokemonTypeList())
    //     dispatchList(GetPokemonList())
    // }, [dispatchTypes, dispatchList]);


    // //pokemonData
    // useEffect(() => {
    //     axios.get(`https://pokeapi.co/api/v2/pokemon?limit=204`).then(function(response){
    //         const { data } = response;
    //         const { results } = data;
    //         const newPokemonData = {}; 
    //         results.forEach((pokemon, index) => {
    //             newPokemonData[index + 1] = {
    //                 id: index + 1,
    //                 name: pokemon.name,
    //                 sprite: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${index+1}.png`,
    //             };
    //         });
    //         setPokemonData(newPokemonData);   
    //     });
    // }, []);