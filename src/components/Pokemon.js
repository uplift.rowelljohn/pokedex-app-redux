import React, { useEffect, useState } from "react";
import { Typography, Link, CircularProgress, Button, Grid} from "@material-ui/core";
import {Chip, Stack, Divider} from '@mui/material';
import Box from '@mui/material/Box';
import DrawerMenu from "./DrawerMenu";
import axios from "axios";
import { makeStyles } from '@material-ui/core/styles';
import { styled } from '@mui/material/styles';

const useStyles = makeStyles(theme => ({

}));

const firstCharToUpper = (name) => {
    return name.charAt(0).toUpperCase() + name.slice(1);
}
const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: `-${drawerWidth}px`,
        ...(open && {
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: 0,
        }),
    }),
);
const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));



 const Pokemon = (props) => {
     const {match, history} = props;
     const {params} = match;
     const {pokemonId} = params;
     const [pokemon, setPokemon] = useState (undefined); 

     useEffect(() => {
        axios
            .get(`https://pokeapi.co/api/v2/pokemon/${pokemonId}`)
            .then(function(response){
                const {data} = response;
                setPokemon(data);
            })
     },[pokemonId]);

     const generatePokemonJSX = () => {
         const {id, name, species, height, weight, types} = pokemon;
         
         //get sprite assets from pokemon.com
         let newID = id.toString().padStart(3, 0);

         const fullImageURL = `https://assets.pokemon.com/assets/cms2/img/pokedex/full/${newID}.png`;

         return(
            <>
                <Typography variant="h3" align="center">
                    {/* {name}{`${id}.`}  */}
                    {firstCharToUpper(name)}
                    <br />
                     <img style={{ width: "300px", height: "300px" }} src={fullImageURL} alt={name} />
                    <Typography variant="h6">Pokemon Info</Typography>
                    <Typography>
                        {"Species: "}
                        <Link >{species.name} </Link>
                    </Typography>
                    <Typography>Height: {height} </Typography>
                    <Typography>Weight: {weight} </Typography>
                    <Typography > Types
                         <Stack direction="row" spacing={1} justifyContent="center" alignItems="center" divider={<Divider orientation="vertical" flexItem />}>
                             {types.map((typeInfo) => {
                                 const { type } = typeInfo;
                                 const { name } = type;
                                 return <Chip label={`${name}`}/>;
                             })}
                         </Stack>
                     </Typography>
                </Typography>
                <br/>
            </>
         )
     }
     const [open] = React.useState(false);
     useStyles();
     return (
         <Box sx={{ display: "flex"}}>
            <DrawerMenu />
            <Main open={open}>
                <DrawerHeader />
                 <Grid align="center" >
                 {pokemon === undefined && <CircularProgress />}
                 {pokemon !== undefined && pokemon && generatePokemonJSX(pokemon)}
                 {pokemon === false && <Typography> Pokemon not found</Typography>}
                 {pokemon !== undefined && (
                     <div >
                             <Button variant="contained" onClick={() => history.goBack()}>
                            back to previous page
                        </Button>
                     </div>
                 )}
                </Grid>
            </Main>
        </Box>
     );
 }

 export default Pokemon;